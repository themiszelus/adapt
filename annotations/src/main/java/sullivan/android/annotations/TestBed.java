package sullivan.android.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by chrissullivan on 9/8/15.
 */
@Retention( RetentionPolicy.SOURCE )
@Target( ElementType.TYPE )
public @interface TestBed
{

}

package sullivan.android.adapt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by chrissullivan on 9/22/15.
 */
public class Bugs
{

    public static String getStackTrace( Exception e )
    {
        Writer string = new StringWriter();
        PrintWriter pr = new PrintWriter( string );

        e.printStackTrace( pr );


        String possibleCause = e.getLocalizedMessage() == null ? "read logcat" : e.getLocalizedMessage();

        pr.append( "\nCaused by : \n =====> " + possibleCause  );

        Throwable cause = e.getCause();
        while( cause != null )
        {
            cause.printStackTrace( pr );
            cause = cause.getCause();
        }

        String result = string.toString();
        pr.close();

        return result;
    }

    public static void showExceptionWindow( Activity activity, Exception e )
    {
        LayoutInflater inflater = ( LayoutInflater ) activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View root = inflater.inflate( R.layout.popup_bug, null );

        BugWindow bg = new BugWindow( root, getStackTrace( e ), activity );
        bg.showAtLocation( activity.getWindow().getDecorView(), Gravity.CENTER, 0, 0 );
    }

    static class BugWindow extends PopupWindow
    {
        private String errors;

        public BugWindow( View contentView, String error, final Activity activity )
        {
            super( contentView, WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true );
            errors = error;

            if( contentView != null )
            {
                TextView logCat = ( TextView ) contentView.findViewById( R.id.logcat );
                logCat.setText( error );

                getContentView()
                        .findViewById( R.id.email )
                        .setOnClickListener( new View.OnClickListener()
                        {
                            @Override
                            public void onClick( View v )
                            {
                                Intent intent = new Intent( Intent.ACTION_SEND );
                                intent.setType( "text/html" );
                                intent.putExtra( Intent.EXTRA_SUBJECT, "Bug" );
                                intent.putExtra( Intent.EXTRA_TEXT, errors );
                                activity.startActivity( intent );

                                dismiss();
                            }
                        } );

                getContentView()
                        .findViewById( R.id.exit )
                        .setOnClickListener( new View.OnClickListener()
                        {
                            @Override
                            public void onClick( View v )
                            {
                                dismiss();
                            }
                        } );
            }
        }
    }

}

package sullivan.android.adapt;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import sullivan.android.annotations.TestBed;
import sullivan.android.annotations.Test;

@TestBed
public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        findViewById( R.id.breakMe ).setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                toast( "My Message", Toast.LENGTH_SHORT );
            }
        } );
    }

    @Test
    public void breakActivity() {
        String pillow = null;
        Log.d( "====>", pillow );
    }

    @Test
    protected void toast( final String someString, final int someInt )
    {
        TextView tv = null;

        tv.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                Toast.makeText( getBaseContext(), someString, someInt ).show();
            }
        } );
    }

}

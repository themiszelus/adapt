package sullivan.android;

import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * Created by chrissullivan on 9/22/15.
 */
public class MethodMeta
{

    protected ExecutableElement mee;

    private String modifier;
    private String returnType;
    private String methodName;
    private String variableTxt;
    private String simpleVariableTxt;
    private String throwables;

    public MethodMeta( ExecutableElement ee )
    {
        this.mee = ee;
        init();
    }

    private void init()
    {
        // remove list to string brackets
        modifier = mee.getModifiers().toString().replaceAll( "\\[|\\]", "" );
        returnType = mee.getReturnType().toString();
        methodName = mee.getSimpleName().toString();

        variableTxt = "";
        List<? extends VariableElement > variables = mee.getParameters();
        for( int i = 0; i < variables.size(); i++ )
        {
            VariableElement ve = variables.get( i );
            variableTxt += ve.asType().toString() + " " + ve.getSimpleName();
            if( i != variables.size() - 1)
            {
                variableTxt += ", ";
            }
        }

        simpleVariableTxt = mee.getParameters().toString();

        throwables = mee.getThrownTypes().toString();
        if( throwables.length() != 0 )
        {
            throwables = "throws " + throwables;
        }
    }

    public boolean isModifiableMethod()
    {
        return modifier.equals( "public" ) || modifier.equals( "protected" );
    }

    public boolean isVoidReturnType()
    {
        return returnType.equals( "void" );
    }

    public String getThrowables()
    {
        return throwables;
    }

    public String getSimpleVariableTxt()
    {
        return simpleVariableTxt;
    }

    public String getVariableTxt()
    {
        return variableTxt;
    }

    public String getMethodName()
    {
        return methodName;
    }

    public String getReturnType()
    {
        return returnType;
    }

    public String getModifier()
    {
        return modifier;
    }

}

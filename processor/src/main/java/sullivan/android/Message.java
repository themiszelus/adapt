package sullivan.android;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Created by chrissullivan on 9/4/15.
 */
public class Message
{
    static Messager mMessager;

    public static void init( Messager messager )
    {
        mMessager = messager;
    }

    public static void note( String msg )
    {
        mMessager.printMessage( Diagnostic.Kind.NOTE, msg );
    }

    public static void warning( String msg )
    {
        mMessager.printMessage( Diagnostic.Kind.WARNING, msg );
    }

    public static void other( String msg )
    {
        mMessager.printMessage( Diagnostic.Kind.OTHER, msg );
    }

    public static void error( String msg )
    {
        mMessager.printMessage( Diagnostic.Kind.ERROR, msg );
    }

    /**
     * Formatting logs
     */

    public static void note( String fmt, Object... args )
    {
        mMessager.printMessage( Diagnostic.Kind.NOTE,
                String.format( fmt, args ) );
    }

    public static void warning( String fmt, Object... args )
    {
        mMessager.printMessage( Diagnostic.Kind.WARNING,
                String.format( fmt, args ) );
    }

    public static void other( String fmt, Object... args )
    {
        mMessager.printMessage( Diagnostic.Kind.OTHER,
                String.format( fmt, args ) );
    }

    public static void error( String fmt, Object... args )
    {
        mMessager.printMessage( Diagnostic.Kind.ERROR,
                String.format( fmt, args ) );
    }

}

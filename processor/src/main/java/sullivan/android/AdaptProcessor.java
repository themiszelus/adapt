package sullivan.android;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

import sullivan.android.annotations.TestBed;

@SupportedAnnotationTypes( {
        "sullivan.android.annotations.TestBed",
        "sullivan.android.annotations.Test"
})
@SupportedSourceVersion( SourceVersion.RELEASE_7 )
public class AdaptProcessor extends AbstractProcessor
{

    @Override
    public synchronized void init( ProcessingEnvironment processingEnv )
    {
        super.init( processingEnv );
        Message.init( processingEnv.getMessager() );
    }

    @Override
    public boolean process( Set< ? extends TypeElement > annotations, RoundEnvironment roundEnv )
    {
        for( Element element : roundEnv.getElementsAnnotatedWith( TestBed.class ) )
        {
            JavaWriter jWriter = new JavaWriter( processingEnv, element );
            jWriter.beginClass( "TestingActivity", element.getSimpleName() + "", new String[0],
                    new String[0] );

            jWriter.overrideMethods();

            jWriter.close();

        }

        return true;
    }

}

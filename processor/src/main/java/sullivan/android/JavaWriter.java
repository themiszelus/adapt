package sullivan.android;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.VariableElement;
import javax.tools.JavaFileObject;

import sullivan.android.annotations.Test;

/**
 * Created by chrissullivan on 9/22/15.
 */
public class JavaWriter
{

    protected ProcessingEnvironment pEnv;
    protected Element mElement;

    protected BufferedWriter bWriter;
    protected JavaFileObject jFo;

    public JavaWriter( ProcessingEnvironment env, Element element )
    {
        this.pEnv = env;
        this.mElement = element;
    }

    public void beginClass( String name, String ext, String[] impl, String[] imports )
    {
        PackageElement pElement = pEnv.getElementUtils().getPackageOf( mElement );
        try
        {
            jFo = pEnv.getFiler().createSourceFile( pElement.getQualifiedName() + "." + name );
            bWriter = new BufferedWriter( jFo.openWriter() );
            bWriter.write( "package " + pElement.getQualifiedName() + ";" );
            newline( 2 );

            if( imports != null )
            {
                for( int i = 0; i < imports.length; i++ )
                {
                    bWriter.write( "import " + imports[i] + ";" );
                    newline( 1 );
                }
            }

            newline( 2 );

            // get extensions classes
            String exte = ext == null ? "" : " extends " + ext + " ";
            String imple = "";

            // get implements classes
            if( impl != null && impl.length > 0 )
            {
                imple += " implements ";
                for( int i = 0; i < impl.length; i++ )
                {
                    imple += impl[i];
                    if( i != impl.length - 1 )
                    {
                        imple += ", ";
                    }
                }
            }

            bWriter.write( "public class " + name + exte + imple + "{" );
            newline( 2 );
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }

    public void overrideMethods()
    {
        List<? extends Element> enclosed = mElement.getEnclosedElements();
        for( Element child : enclosed )
        {
            if( child.getAnnotation( Test.class ) != null )
            {
                ExecutableElement ee = ( ExecutableElement ) child;
                MethodMeta meta = new MethodMeta( ee );

                if( !meta.isModifiableMethod() )
                {
                    continue;
                }

                String output = "";

                if( meta.isVoidReturnType() )
                {
                    output = String.format( "    @Override\n" +
                                    "    public void %s( %s ) %s\n" +
                                    "    {\n" +
                                    "        try\n" +
                                    "        {\n" +
                                    "            super.%s( %s );\n" +
                                    "        }\n" +
                                    "        catch( Exception e )\n" +
                                    "        {\n" +
                                    "            Bugs.showExceptionWindow( this, e );\n" +
                                    "        }\n" +
                                    "    }\n",
                            meta.getMethodName(), meta.getVariableTxt(), meta.getThrowables(),
                            meta.getMethodName(), meta.getSimpleVariableTxt() );
                }
                else
                {
                    output = String.format( "    @Override\n" +
                                    "    public %s %s( %s ) %s\n" +
                                    "    {\n" +
                                    "        try\n" +
                                    "        {\n" +
                                    "            %s val = super.%s( %s );\n" +
                                    "            return val;\n" +
                                    "        }\n" +
                                    "        catch( Exception e )\n" +
                                    "        {\n" +
                                    "            Bugs.showExceptionWindow( this, e );\n" +
                                    "            return null;\n" +
                                    "        }\n" +
                                    "    }\n",
                            meta.getReturnType(), meta.getMethodName(), meta.getVariableTxt(),
                            meta.getThrowables(), meta.getReturnType(),
                            meta.getMethodName(), meta.getSimpleVariableTxt() );
                }

                try
                {
                    bWriter.write( output );
                    bWriter.newLine();
                }
                catch ( IOException e )
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean validModifier( String modifier )
    {
        return modifier.equals( "public" ) || modifier.equals( "protected" );
    }

    public boolean isVoidReturnType( String typer )
    {
        return typer.equals( "void" );
    }

    public void addChunk( String data )
    {
        String[] lines = data.split( "\\n" );
        for( String line : lines )
        {
            try
            {
                bWriter.write( line );
                newline( 1 );
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

    public void close()
    {
        try
        {
            newline( 1 );
            bWriter.write( "}" );
            bWriter.close();
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }

    public void newline( int amount )
    {
        if( bWriter == null ) return;

        for( int i = 0; i < amount; i++ )
        {
            try
            {
                bWriter.newLine();
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

}
